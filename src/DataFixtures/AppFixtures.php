<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Famille;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $tabFam = ["Livres", "Multimédia", "Cierges"];

        $faker = Factory::create('fr_FR');

        foreach ($tabFam as $nomFamille) {
            $famille = new Famille();
            $famille->setNom($nomFamille);
            $manager->persist($famille);

            for ($i=0; $i < 10; $i++) { 
                $art = new Article();
                $art->setFamille($famille)
                ->setPrix(rand(100,40000))
                ->setPhoto("https://picsum.photos/200/200")
                ->setNom($faker->realText(30))
                ->setDescr($faker->realTextBetween(100,200));
                $manager->persist($art);
            }

        }

        $manager->flush();
    }
}
