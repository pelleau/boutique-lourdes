<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController {

    #[Route("/", name:"accueil")]
    public function accueil(ArticleRepository $repo)
    {
        $articles = $repo->findBy([],null,3);

        return $this->render('accueil.html.twig',[
            'meilleurs'=>$articles
        ]);
    }

}