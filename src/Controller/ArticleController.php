<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\FamilleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    #[Route('/fam/{idf}', name: 'article.famille')]
    public function famille( FamilleRepository $repo,  $idf): Response
    {
        $famille = $repo->find($idf);

        if ($famille==null) {
            return $this->redirectToRoute('accueil');
            // throw $this->createNotFoundException('Tu lui veux quoi à cette famille ?');
        }

        return $this->render('article/famille.html.twig', [
            'famille' => $famille
        ]);
    }

    #[Route('/art/{ida}', name: 'article.detail')]
    public function detail( ArticleRepository $repo, $ida): Response
    {
        $article = $repo->find($ida);

        if ($article==null) {
            return $this->redirectToRoute('accueil');
        }

        return $this->render('article/detail.html.twig', [
            'article' => $article
        ]);
    }

}
